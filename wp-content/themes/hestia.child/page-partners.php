<?php
/**
 * The template for displaying all single posts and attachments.
 *
 * @package Hestia
 * @since Hestia 1.0
 */
/*
Template Name: Modèle de Page Partners
*/

get_header();

do_action( 'hestia_before_single_page_wrapper' );
?>
<div class="container">
<?php 
    query_posts(array( 
        'post_type' => 'partenaire',
        'showposts' => 10 
    ) );  
?>
<?php while (have_posts()) : the_post(); ?>
        <div class="card card-body" style= 'width : 20rem'>
            <?php the_post_thumbnail(); ?>
            <div class="text">
                <h2><b><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></b></h2>
                <p><i>Partenaire proposée par : <?php the_author(); ?></i></p>
                <p><?php echo get_the_excerpt(); ?></p>
            </div>
        </div>
<?php endwhile;?>
</div>
<?php get_footer(); ?>
